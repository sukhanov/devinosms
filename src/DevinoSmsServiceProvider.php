<?php
namespace Sukhanov\Devinosms;

use Illuminate\Support\ServiceProvider;

class DevinoSmsServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/devino.php' => app()->basePath() . '/config' . '/' . 'devino.php'
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }

}