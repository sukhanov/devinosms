<?php

namespace Sukhanov\Devinosms;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class Sms extends Controller {


    /**
     * SMS sending method
     * See API documentation: http://docs.devinotele.com/httpapi.html#sms
     * @param $phoneNumber
     * @param $smsText
     * @return string
     */
    public static function send($phoneNumber, $smsText)
    {
        // Get session ID
        $sessionId = json_decode(self::getSessionId());

        if (! property_exists($sessionId, 'sessionId')){
            return json_encode($sessionId);
        }

        $url = config('devino.rest_platform') . "/Sms/Send";

        $client = new Client();

        try {
            $response = $client->post($url, [
                'query' => [
                    'sessionId' => $sessionId->sessionId,
                    'sourceAddress' => config('devino.source_address'),
                    'destinationAddress' => $phoneNumber,
                    'data' => $smsText
                ]
            ]);
        } catch (ClientException $e){
            // In case of any errors API will return response like
            // { Code: 4, Desc: "Invalid user login or password"}
            return json_encode($e->getResponse()->getBody()->getContents());
        }

        if ($response->getStatusCode() == 200 && $response->getReasonPhrase() == 'OK'){
            return json_encode(["Code" => 200, "Desc" => 'SMS was sent successfully!']);
        }

    }
    
    
    /**
     * SMS sending method to array of recipients
     * See API documentation: http://docs.devinotele.com/httpapi.html#id6
     * @param $phoneNumbers
     * @param $smsText
     * @return string
     */
    public static function multiSend($phoneNumbers, $smsText)
    {
        // Get session ID
        $sessionId = json_decode(self::getSessionId());

        if (! property_exists($sessionId, 'sessionId')){
            return json_encode($sessionId);
        }

        $url = config('devino.rest_platform') . "/Sms/SendBulk";

        // We have to persuade that $phoneNumbers is an not empty array
        if ( ! is_array($phoneNumbers) || count($phoneNumbers) == 0){
            return json_encode(["Code" => 500, "Desc" => 'Invalid recepients!']);
        }

        $client = new Client();

        $requestParams = http_build_query([
            'SessionID' => $sessionId->sessionId,
            'SourceAddress' => config('devino.source_address'),
            'Data' => $smsText
        ]);

        // Add all phones in query string
        foreach ($phoneNumbers as $phoneNumber) {
            $requestParams .= "&DestinationAddresses=".$phoneNumber;
        }

        try {
            $response = $client->post($url, [
                'query' => $requestParams
            ]);
        } catch (ClientException $e){
            // In case of any errors API will return response like
            // { Code: 4, Desc: "Invalid user login or password"}
            return json_encode($e->getResponse()->getBody()->getContents());
        }

        if ($response->getStatusCode() == 200 && $response->getReasonPhrase() == 'OK'){
            return json_encode(["Code" => 200, "Desc" => 'Messages were sent successfully!']);
        }

    }


    /**
     * Method receives session ID
     * See API documentation: http://docs.devinotele.com/httpapi.html#id2
     * @return string
     */
    public static function getSessionId()
    {
        // Build request string to get session id
        $url = config('devino.rest_platform') . '/User/SessionId?login=' ;
        $url .= config('devino.login') . '&password=' . config('devino.password');

        $client = new Client();

        try {
            $response = $client->get($url);
        } catch (ClientException $e){
            // In case of any errors API will return response like
            // { Code: 4, Desc: "Invalid user login or password"}
            return json_encode($e->getResponse()->getBody()->getContents());
        }

        if ($response->getStatusCode() == 200 && $response->getReasonPhrase() == 'OK'){

            $sessionId = str_replace('"', '', $response->getBody()->getContents());

            if (strlen($sessionId) != 36) {
                return json_encode(["Code" => 500, "Desc" => "Invalid sessionId"]);
            }

            return json_encode(["Сode" => 200, "sessionId" => $sessionId]);
        }
    }
}