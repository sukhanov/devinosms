<?php
/**
 * The config file provides authorisation data
 * for devino SMS gate
*/

return [
    // Login and pass to obtain session id
    'login' => 'test',
    'password'  => 'test',

    // Addres from which to send sms
    'source_address' => 'Sender',

    // Platform to which we have to make rest requests
    'rest_platform' => 'https://integrationapi.net/rest',
];
