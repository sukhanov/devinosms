# Devinosms

Current package provides SMS sending via [ API ](https://devinotele.com/en/resheniya/dokumentaciya-po-api/HTTPSREST-protocol/API_Overview/) devinotele.com. This package can be used in Laravel 5.4 or higher with PHP 5.6 or higher.

## Installation

You can install the package via composer:

```shell
> composer require sukhanov/devinosms
```

### Laravel

Register `DevinoSmsServiceProvider` in config/app.php

```php
    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [
    

        ...
        
        \Sukhanov\Devinosms\DevinoSmsServiceProvider::class,

    ],

```

### Lumen

Register `DevinoSmsServiceProvider` in bootstrap/app.php

```php
$app->register(\Sukhanov\Devinosms\DevinoSmsServiceProvider::class);

```

## Configuration

### Laravel

In order to publish configuration file execute the command below

```shell

> php artisan vendor:publish --provider="Sukhanov\Devinosms\DevinoSmsServiceProvider"

```

### Lumen

Please copy configuration file manually in case you did't setup artisan commands for Lumen. It's located in 
`vendor/sukhanov/devinosms/src/config/devino.php`. 


### Config sample

Edit configuration file with your authorisation data

```php
<?php
/**
 * The config file provides authorisation data
 * for devino SMS gate
*/

return [
    // Login and pass to obtain session id
    'login' => 'test',
    'password'  => 'test',

    // Addres from which to send sms
    'source_address' => 'Sender',

    // Platform to which we have to make rest requests
    'rest_platform' => 'https://integrationapi.net/rest',
];

```

## Usage

```php

<?php

namespace App\Http\Controllers;

use Sukhanov\Devinosms\Sms;

class SomeController extends Controller
{
    public function index()
    {
        // Sending a message via 'send' method to one recipient
        Sms::send('+79000000000', 'laravel test');
        // Sending a message to a list of recipients
        // Note: please make sure that list od phone numbers is corrent and all items are valid phone numbers
        Sms::multiSend(['+79000000000','+79000000001'], 'laravel test');
    }
}

```

### Responses examples
 In case of any problems response will look like

```json

{"Code":4,"Desc":"Invalid user login or password"}

```
In case if SMS was sent we will get `"Code":200`

```json

{"Code":200,"Desc":"SMS was sent successfully!"}

```









